﻿using UnityEngine;
using System.Collections;

public class SCRIPT : MonoBehaviour
{

    GameObject cube;
    public Transform centro;
    private Vector3 eje = Vector3.up;
    private Vector3 posicion;
    public float radio = 2.0f;
    private float velocidadradio = 0.5f;
    private float velocidadrotacion = 80.0f;

    void Start()
    {
        transform.position = (transform.position - centro.position).normalized * radio + centro.position;
        radio = 2.0f;
    }

    void Update()
    {
        transform.RotateAround(centro.position, eje, velocidadrotacion * Time.deltaTime);
        posicion = (transform.position - centro.position).normalized * radio + centro.position;
        transform.position = Vector3.MoveTowards(transform.position, posicion, Time.deltaTime * velocidadradio);
    }
}





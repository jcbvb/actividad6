﻿using UnityEngine;

public class CapsuleMovement : MonoBehaviour {

    // Capsule speed when it's moving
    public float speed = 5f;

	// Use this for initialization
	void Start () {
        // Move five unity space units per second towards front direction (only once at initialization)
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        Debug.Log("Moving From Start Event");
	}
	
	// Update is called once per frame
	void Update () {
        // Move five unity space units per second towards right direction (executed every frame)
        transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        Debug.Log("Moving From Update Event");
    }
}


